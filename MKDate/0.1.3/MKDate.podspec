Pod::Spec.new do |s|
  s.name             = 'MKDate'
  s.version          = '0.1.3'
  s.summary          = 'MKSuites Library for working with dates.'
  s.description      = <<-DESC
MKDate is made for manipulating and working with dates in swift.
                       DESC
  s.homepage         = 'https://lachlangrant@bitbucket.org/lachlangrant/MKDate'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'lachlangrant' => 'lachlangrant@rbvea.co' }
  s.source           = { :git => 'https://bitbucket.org/lachlangrant/mkdate.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'
  s.source_files = 'MKDate/Classes/**/*'
  s.resources = 'MKDate/Assets/MKDate.bundle'
end
