Pod::Spec.new do |s|
  s.name             = 'MKRouter'
  s.version          = '0.1.1'
  s.summary          = 'A short description of MKRouter.'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/lachlangrant/MKRouter'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Lachlan Grant' => 'lachlangrant@rbvea.co' }
  s.source           = { :git => 'https://lachlangrant@bitbucket.org/lachlangrant/MKRouter.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'
  s.source_files = 'MKRouter/Classes/**/*'
  s.frameworks = 'UIKit'
end
