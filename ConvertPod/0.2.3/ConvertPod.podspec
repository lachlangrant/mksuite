Pod::Spec.new do |s|
  s.name             = 'ConvertPod'
  s.version          = '0.2.3'
  s.summary          = 'Supporting Pod for ConvertNow'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
  s.homepage         = 'https://bitbucket.org/lachlangrant/convertpod'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Lachlan Grant' => 'lachlangrant@rbvea.co' }
  s.source           = { :git => 'https://lachlangrant@bitbucket.org/lachlangrant/convertpod.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'
  s.source_files = 'ConvertPod/Classes/**/*'
  s.dependency 'MKUtility', '~> 1.0.2'
  s.dependency 'MKConstants', '~> 0.1.3'
end
