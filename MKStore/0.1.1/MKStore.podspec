Pod::Spec.new do |s|
  s.name             = 'MKStore'
  s.version          = '0.1.1'
  s.summary          = 'IAP Library'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
  s.homepage         = 'https://bitbucket.org/lachlangrant//MKStore'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Lachlan Grant' => 'lachlangrant@rbvea.co' }
  s.source           = { :git => 'https://lachlangrant@bitbucket.org/lachlangrant/MKStore.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'
  s.source_files = 'MKStore/Classes/**/*'
  s.dependency 'MKUtility', '~> 1.0.2'
  s.dependency 'MKConstants', '~> 0.1.3'
  s.dependency 'SwiftyStoreKit', '~> 0.12.0'
end
